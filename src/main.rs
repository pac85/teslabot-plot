mod serve;

#[macro_use]
extern crate lazy_static;
extern crate serde_json;
extern crate serde;
extern crate rand;
extern crate toml;

use std::net::{TcpListener, TcpStream};
use std::io::{Read, Write};
use std::fs::File;
use std::process::{Command, Stdio};
use std::sync::Arc;
use std::thread; 

use serde::{Serialize, Deserialize};

use rand::{thread_rng, Rng};
use rand::distributions::Alphanumeric;

#[derive(Deserialize)]
struct PlotRequest
{
    output_type: u8,
    octave_expression: String
}

#[derive(Deserialize, Clone)]
pub struct Config
{
    web_address:        String,
    web_root:           String,
    command_address:    String,
}

fn random_string(len: usize) -> String
{
    thread_rng()
        .sample_iter(&Alphanumeric)
        .take(len)
        .collect()
}

fn handle_request(req: PlotRequest, config: &Config) ->  String
{
    let output_types = ["pdf", "png"];
    if output_types.len() <= req.output_type as usize
    {
        println!("invalid format");
        return "".to_owned();
    }

    let mut rchild = Command::new("octave")
        .stdin(Stdio::piped())
        .spawn();

   let filename = random_string(10);
    if let Ok(mut child) = rchild
    {
       if let Some(stdin) = child.stdin.as_mut()
       {
           stdin.write_all((format!("{}; graphics_toolkit(\"gnuplot\"); print \"{}/{}.{}\"", req.octave_expression, config.web_root, filename, output_types[req.output_type as usize])).as_bytes());
       }
    }
    filename
}

lazy_static!
{
    static ref CONFIG: Config = {
        let mut config_file = File::open("config.toml").expect("Unable to find configuration file");
        let mut config_data= String::new();
        config_file.read_to_string(&mut config_data).expect("Unable to read configuration file");
        toml::from_str(&config_data).expect("Unable to parse configuraation file")
    };   
}

fn main() 
{
    let config = CONFIG.clone();

    thread::spawn(move || {
        serve::serve(& CONFIG);
    });

    let listener = TcpListener::bind(config.command_address.clone()).unwrap();
    println!("ready");
    for stream in listener.incoming() {
        println!("accepted connection");
        if let Ok(mut stream) = stream
        {
            let mut data = Vec::new();
            loop
            {
                let mut t_byte = [0u8];
                stream.read(&mut t_byte);
                data.push(t_byte[0]);
                if t_byte[0] == '}' as u8
                {
                    break;
                }
            }
            let request: Result<PlotRequest, _> = serde_json::from_str(&String::from_utf8_lossy(&data));
            if let Ok(request) = request
            {
                stream.write_all(handle_request(request, &config).as_bytes());
            }
            else
            {
                println!("invalid request format");
            }
        }
    }
}
