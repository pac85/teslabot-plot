extern crate hyper;

use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use std::str::FromStr;
use std::fs::File;
use std::io::Read;

use hyper::{Body, Response, Server};
use hyper::service::service_fn_ok;
use hyper::rt::Future;
use hyper::HeaderMap;
use hyper::header;

fn get_content_type(ext: &str) -> &'static str
{
    match(ext)
    {
        "pdf" => "application/pdf",
        "png" => "image/png",
        _ => "application/octet-stream"
    }
}

pub fn serve(conf: &'static super::Config)
{
    let addr = SocketAddr::from_str(&conf.web_address).expect("invalid web address");//(ip, port).into();

    // And a MakeService to handle each connection...
    let make_service = || {
        service_fn_ok(|_req| {
            let filename = _req.uri().path();
            let path = format!("{}/{}", super::CONFIG.web_root, filename);
            let file_extension = filename.split('.').last().unwrap_or("");

            if filename == "/"
            {
                return Response::builder()
                    .status(404)
                    .body(Body::from(r"<b><h1>404 not found</h1></b>
                                     What are you doing here, go <a href='https://t.me/theBestTeslaBot'>away</a>"))
                    .unwrap();
            }

            if let Ok(mut file) = File::open(path)
            {
                let mut response_data = Vec::new();
                file.read_to_end(&mut response_data);

                return Response::builder()
                    .status(200)
                    .header("content-type", get_content_type(file_extension))
                    .body(Body::from(response_data))
                    .unwrap()
            }

            Response::builder()
                .status(404)
                .body(Body::from(r" <b><h1>404 not found</h1></b>
                                 The requested resource could not be found,
                                 <b>this might be because the link has expired</b>,
                                 please send the expression to <a href='https://t.me/theBestTeslaBot'>the bot</a> again and use the new link"))
                .unwrap()
        })
    };

    // Then bind and serve...
    let server = Server::bind(&addr)
        .serve(make_service);

    // Finally, spawn `server` onto an Executor...
    hyper::rt::run(server.map_err(|e| {
        eprintln!("server error: {}", e);
    }));
}
